# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta
from .stock import (ShipmentCSVMixin, ShipmentCSVProductMixin,
    ShipmentCSVLocationMixin)

__all__ = ['ShipmentOutReturn', 'ShipmentIn', 'ShipmentInProduct',
    'ShipmentOutReturnProduct', 'ShipmentInternal']


class ShipmentOutReturn(ShipmentCSVMixin, metaclass=PoolMeta):
    __name__ = 'stock.shipment.out.return'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls._csv_move_field = 'incoming_moves'

    @classmethod
    def _get_csv_field_extra_domain(cls, field_name, data, row=[]):
        domain = super()._get_csv_field_extra_domain(field_name, data, row=row)
        if field_name == 'delivery_address':
            domain.append(('party', '=', data['customer'].id))
        return domain

    @classmethod
    def _get_csv_key(cls):
        return ('customer', ) + super()._get_csv_key()

    def _set_csv_move_locations(self, move):
        move.from_location = self.on_change_with_customer_location()
        move.to_location = self.on_change_with_warehouse_input()


class ShipmentIn(ShipmentCSVMixin, metaclass=PoolMeta):
    __name__ = 'stock.shipment.in'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls._csv_move_field = 'incoming_moves'

    @classmethod
    def _get_csv_field_extra_domain(cls, field_name, data, row=[]):
        domain = super()._get_csv_field_extra_domain(field_name, data, row=row)
        if field_name == 'contact_address':
            domain.append(('party', '=', data['supplier'].id))
        return domain

    @classmethod
    def _get_csv_key(cls):
        return ('supplier', 'reference', 'warehouse', )

    def _set_csv_move_locations(self, move):
        move.from_location = self.on_change_with_supplier_location()
        move.to_location = self.on_change_with_warehouse_input()


class ShipmentOutReturnProduct(ShipmentCSVProductMixin, metaclass=PoolMeta):
    __name__ = 'stock.shipment.out.return'

    @classmethod
    def _get_cross_reference_pattern(cls, data):
        return {
            'party': data['customer'].id if data['customer'] else None,
            'address': data['delivery_address'].id
                if data['delivery_address'] else None
        }


class ShipmentInProduct(ShipmentCSVProductMixin, metaclass=PoolMeta):
    __name__ = 'stock.shipment.in'

    @classmethod
    def _get_cross_reference_pattern(cls, data):
        return {
            'party': data['supplier'].id if data['supplier'] else None,
            'address': data['contact_address'].id
                if data['contact_address'] else None
        }


class ShipmentInLocation(ShipmentCSVLocationMixin, metaclass=PoolMeta):
    __name__ = 'stock.shipment.in'

    @classmethod
    def _get_cross_reference_pattern(cls, data):
        return {
            'party': data['supplier'].id if data['supplier'] else None,
            'address': data['contact_address'].id
                if data['contact_address'] else None
        }


class ShipmentInternal(ShipmentCSVMixin, metaclass=PoolMeta):
    __name__ = 'stock.shipment.internal'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls._csv_move_field = 'incoming_moves'

    @classmethod
    def _get_csv_key(cls):
        return ('to_location', 'from_location', ) + super()._get_csv_key()

    def _set_csv_move_locations(self, move):
        move.from_location = self.from_location
        move.to_location = self.to_location
