===========================================
Stock Shipment Internal Csv Import Scenario
===========================================

Imports::

    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.stock_csv_import.tests.tools import read_csv_file
    >>> import os
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from decimal import Decimal

Install stock_csv_import::

    >>> config = activate_modules('stock_csv_import')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create Configuration::

    >>> Configuration = Model.get('stock.configuration')
    >>> config = Configuration()
    >>> config.shipment_internal_csv_headers = "reference,effective_date,from_location.name,to_location.name,incoming_moves.product.id,incoming_moves.quantity"
    >>> config.save()

Create Product::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> product = Product()
    >>> template = ProductTemplate()
    >>> template.name = 'Product 1'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.purchasable = True
    >>> template.list_price = Decimal('10')
    >>> template.cost_price = Decimal('5')
    >>> template.cost_price_method = 'fixed'
    >>> template.save()
    >>> product.template = template
    >>> product.save()

Create locations::

    >>> Location = Model.get('stock.location')
    >>> sto_loc1 = Location(name='Storage 1')
    >>> sto_loc1.code = 'STO'
    >>> sto_loc1.type = 'storage'
    >>> sto_loc1.save()
    >>> sto_loc2 = Location(name='Storage 2')
    >>> sto_loc2.code = 'STO'
    >>> sto_loc2.type = 'storage'
    >>> sto_loc2.save()

CSV import wizard::

    >>> ShipmentInternal = Model.get('stock.shipment.internal')
    >>> Data = Model.get('ir.model.data')
    >>> data, = Data.find([
    ...     ('module', '=', 'stock_csv_import'),
    ...     ('fs_id', '=', 'wizard_shipment_internal_csv_import')])
    >>> csv_import = Wizard('stock.shipment.csv_import', action={'id': data.db_id})
    >>> filename = os.path.join(os.path.dirname(__file__), 'internal.csv')
    >>> csv_import.form.csv_file = read_csv_file(filename)
    >>> csv_import.form.store_file = False
    >>> csv_import.execute('import_')
    >>> shipment, = ShipmentInternal.find([])
    >>> shipment.effective_date
    datetime.date(2020, 4, 7)
    >>> shipment.from_location == sto_loc1
    True
    >>> shipment.to_location == sto_loc2
    True
    >>> move, = shipment.moves
    >>> move.quantity
    50.0
    >>> move.product.name
    'Product 1'
    >>> move.from_location == shipment.from_location
    True
    >>> move.to_location == shipment.to_location
    True

CSV import wizard when location does not exist::

    >>> csv_import = Wizard('stock.shipment.csv_import', action={'id': data.db_id})
    >>> filename = os.path.join(os.path.dirname(__file__), 'internal_error.csv')
    >>> csv_import.form.csv_file = read_csv_file(filename)
    >>> csv_import.execute('import_')
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: Cannot find a From Location record with value "Storage Error" for CSV file line number "1". - 