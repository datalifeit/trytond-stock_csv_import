# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.model import fields

__all__ = ['Configuration']


class Configuration(metaclass=PoolMeta):
    __name__ = 'stock.configuration'

    shipment_out_return_csv_headers = fields.Char(
        'Shipment out Return CSV Headers')
    shipment_in_csv_headers = fields.Char(
        'Shipment in CSV Headers')
    shipment_internal_csv_headers = fields.Char(
        'Shipment interal CSV Headers')

    @classmethod
    def __register__(cls, module_name):
        table = cls.__table_handler__(module_name)

        # Migration from 5.0: rename fields
        if table.column_exist('out_return_csv_headers'):
            table.column_rename('out_return_csv_headers',
                'shipment_out_return_csv_headers')

        if table.column_exist('csv_headers'):
            table.column_rename('csv_headers', 'shipment_in_csv_headers')
            table.drop_column('csv_headers')

        super().__register__(module_name)
